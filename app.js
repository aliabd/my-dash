if (localStorage.getItem("widgets") === null)
  localStorage.setItem('widgets','[{"type":"Reddit","subreddit":"worldnews","sort":"hot", "speed":10, "size":12}]')

if (localStorage.getItem("backgrounds") === null)
  localStorage.setItem('backgrounds','{"source":"wallpaper","speed":60}')

var widgets = JSON.parse(localStorage.getItem("widgets"))
var backgrounds = JSON.parse(localStorage.getItem("backgrounds"))
var OWMKey = '2db56163b69c6c28ff15634d9d479689'

Vue.component('reddit-box', {
  template:'#reddit-box-template',
  data: function() {
    return {
      count: 0,
      posts: []
    }
  },
  props: [ 'subreddit' , 'sorting' , 'speed', 'size'],
  watch: {
    count: function() {
      resizeText(this.$el.firstChild.firstChild.childNodes[2])
    }
  },
  methods: {
    fetchData: function(){
        var vm = this
        fetchJson(`https://www.reddit.com/r/${this.subreddit}/${this.sorting}.json`)
        .then(function(posts) {
          vm.posts = posts.data.children
        })
      }
  },
  created: function () {
    startUpdate(this)
    startCycle(this)
  },
  mounted: function() {
    resizeWidgets()
    resizeText(this.$el.firstChild.firstChild.childNodes[2], 50, 5, 500)
  },
  destroyed: function() {
    resizeWidgets()
  }
})

Vue.component('rss-box', {
  template: '#rss-box-template',
  data: function() {
    return {
      count: 0,
      posts: []
    }
  },
  props: [ 'rss' , 'speed', 'size'],
  computed: {
    host: function() {
      return new URL(this.rss).hostname
    }
  },
  watch: {
    count: function() {
      resizeText(this.$el.firstChild.firstChild.childNodes[2])
    }
  },
  methods: {
    fetchData: function(){
        var vm = this
        fetchJson(`https://api.rss2json.com/v1/api.json?rss_url=${this.rss}`)
        .then(function(posts) {
          vm.posts = posts.items
        })
      }
  },
  created: function () {
    startUpdate(this)
    startCycle(this)
  },
  mounted: function() {
    resizeWidgets()
    resizeText(this.$el.firstChild.firstChild.childNodes[2], 24, 5, 500)
  },
  destroyed: function() {
    resizeWidgets()
  }
})

Vue.component('weather-box', {
  template: '#weather-box-template',
  data: function() {
    return {
      count: 0,
      url: '',
      image: true,
      data: []
    }
  },
  computed: {
    imgURL : function() {
      return Object.keys(this.data).length > 0 ? "http://openweathermap.org/img/w/" +
      this.data.weather[0].icon + ".png" : ''
    }
  },
  props: [ 'city', 'update', 'size'],
  methods: {
    fetchData: function(){
      var vm = this
      fetchJson(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&APPID=${OWMKey}`)
      .then(function(data) {
        vm.data = data
        vm.url = `https://openweathermap.org/city/${data.id}`
      })
    },
/*  showImage: function() {
        if (this.$el.scrollHeight < 150)
          this.$el.firstChild.firstChild.childNodes[2].firstChild.style.display = 'none'
        else
          this.$el.firstChild.firstChild.childNodes[2].firstChild.style.display = 'block'
    }
*/
  },
  created: function () {
    startUpdate(this)
  },
  mounted: function() {
    //window.addEventListener('resize', this.showImage)
    //this.showImage()
    resizeWidgets()
    resizeText(this.$el.firstChild.firstChild.childNodes[2], 24, 5, 500)
  },
  destroyed: function() {
    resizeWidgets()
  }
})

Vue.component('size-option-template', {
  template: '#weather-box-template'
})

var app = new Vue({
    el: '#app',
    data: {
      time: timeNow(),
      widgets: widgets,
      showNav: false,
      showBgSettings: false,
      background_url: 'http://news.slyloop.com/background.jpg',
      backgrounds: [],
      background_subreddit: backgrounds.source,
      background_speed: backgrounds.speed,
      type: '',
      widget_size: 4,
      subreddit_name: '',
      sorting: 'Hot',
      weather_city: '',
      rss_url: '',
      speed: 10
    },
    computed: {
      arrow_class: function() {return this.showBgSettings ? 'ti-angle-up' : 'ti-angle-down'},
      arrow_style: function() {return this.showBgSettings ? 'padding-top: 1.2em' : ''},
      manager_height: function() {
        var base = 380
        if (this.type !== '') {
          if (this.type === 'Reddit') base += 350
          else base += 280
        }
        if (this.showBgSettings) base += 220
        return base + 'px'
      }
    },
    watch: {
      widgets: function (widgets) {
        localStorage.setItem('widgets',JSON.stringify(this.widgets))
      }
    },
    mounted: function() {
      setInterval(() => this.time = timeNow(), 500)
      this.fetchBackgrounds()
      var vm = this
      var count = 0
      setTimeout(function cycle() {
        count = (count + 1) % vm.backgrounds.length
        if (vm.backgrounds[count].data.url.split('//')[1].split('.')[0] !== 'i')
            count = (count + 1) % vm.backgrounds.length
        vm.background_url = vm.backgrounds[count].data.url
        setTimeout(cycle, vm.background_speed * 1000)
      }, vm.background_speed * 1000)
      resizeWidgets()
    },
    methods: {
      closeNav: function(e) {
        if (e.target !== document.getElementById('settings')
        && !document.getElementById('nav-panel').contains(e.target)) {
          this.showNav = false
        }
      },
      addRedditWidget: function() {
        this.widgets.push(
        {
          type:'Reddit',
          size: this.widget_size,
          subreddit:this.subreddit_name.toLowerCase(),
          sort:this.sorting.toLowerCase(),
          speed:this.speed
        })
        this.type = ''
      },
      addRSSWidget: function() {
        this.widgets.push(
        {
          type:'RSS',
          size: this.widget_size,
          url:this.rss_url,
          host:new URL(this.rss_url).hostname,
          speed:this.speed
        })
        this.type = ''
      },
      addWeatherWidget: function() {
        this.widgets.push(
        {
          type:'Weather',
          size: this.widget_size,
          city:this.weather_city,
          speed:this.speed
        })
        this.type = ''
      },
      deleteWidget: function(e) {
        this.widgets.splice(e.target.parentNode.dataset.id, 1)
      },
      reorderUp: function(e) {
        move(this.widgets, e.target.parentNode.dataset.id, -1)
      },
      reorderDown: function(e) {
        move(this.widgets, e.target.parentNode.dataset.id, 1)
      },
      updateBackgroundSource: function() {
        this.showBgSettings = false
        localStorage.setItem('backgrounds',JSON.stringify(
        {
          source:this.background_subreddit,
          speed:this.background_speed
        }))
        this.fetchBackgrounds()
      },
      fetchBackgrounds: function(){
        var vm = this
        fetchJson(`http://www.reddit.com/r/${this.background_subreddit}/top/.json`)
        .then(function(response) {
          vm.backgrounds = response.data.children
        })
      }
    }
})

function move(array, index, change) {
  var newIndex = parseInt(index) + change
  if (newIndex < 0  || newIndex == array.length) return array
  var indexes = [index, newIndex].sort()
  array.splice(indexes[0], 2, array[indexes[1]], array[indexes[0]])
}

function fetchJson(url) {
  return fetch(url).then(function(response) {
    return response.json()
  })
}

function startCycle(vm) {
  setTimeout(function cycle() {
    vm.count += 1
    vm.count %= vm.posts.length
    setTimeout(cycle, vm.speed * 1000)
  }, vm.speed * 1000)
}

function startUpdate(vm) {
  vm.fetchData()
  var update = vm.update === undefined ? 30 : vm.update
  update *= 60000
  setTimeout(function cycle() {
    vm.fetchData()
    setTimeout(cycle, update)
  }, update)
}

function timeNow() {
  var time = new Date()
  var zero = (i) => {return i < 10 ? '0' + i : i}
  return zero(time.getHours()) + ':' + zero(time.getMinutes()) + ':' + zero(time.getSeconds())
}

function resizeWidgets() {
  var box_height=  Math.round((window.innerHeight - 300)/
    Math.round(document.getElementById('content').clientHeight/
    document.getElementsByClassName('box')[0].clientHeight))
  var widgets = document.querySelectorAll('.box')
  for (var i = 0; i < widgets.length; i++) {
    widgets[i].style.height = box_height + 'px'
  }
}

window.onresize = function() {
  resizeWidgets()
}

async function resizeText(el, maxSize = 50, minSize = 5, delay = 0) {
  await sleep(delay)
  el.style.fontSize = maxSize + 'px'
  for (var i = maxSize; i > minSize; i--) {
    if (el.scrollHeight > el.clientHeight)
      el.style.fontSize = parseInt(el.style.fontSize) - 1 + 'px'
    else break
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}